class GameField {
    x;
    o;
  constructor() {
    this.state = [
      [0, 0, 0],
      [0, 0, 0],
      [0, 0, 0]
    ];
    this.x = "x";
    this.o = "o";
    this.isOverGame = false;
  }

  getGameFieldStatus() {
    return this.state;
  }
 
  setMode(mode) {
    this.mode = mode;
  }

  FieldCellValue(row, col) {
    if (this.state[row][col] !== 0) {
      alert("Эта ячейка уже занята!");
      return false;
    } else {
      this.state[row][col] = this.mode;
      return this.mode
    }  
  }

  checkForWinner() {
    const rows = this.state;
    const cols = [[], [], []];
    const diagonals = [[], []];
    let winner = null;

    // строки
    for (let i = 0; i < 3; i++) {
      if (rows[i][0] === rows[i][1] && rows[i][0] === rows[i][2]) {
        winner = rows[i][0];
        break;
      }
    }

    // колонки
    if (!winner) {
      for (let i = 0; i < 3; i++) {
        cols[0].push(rows[0][i]);
        cols[1].push(rows[1][i]);
        cols[2].push(rows[2][i]);
      }

      for (let i = 0; i < 3; i++) {
        if (cols[i][0] === cols[i][1] && cols[i][0] === cols[i][2]) {
          winner = cols[i][0];
          break;
        }
      }
    }

    // диагонали
    if (!winner) {
      diagonals[0].push(rows[0][0]);
      diagonals[0].push(rows[1][1]);
      diagonals[0].push(rows[2][2]);
      diagonals[1].push(rows[2][0]);
      diagonals[1].push(rows[1][1]);
      diagonals[1].push(rows[0][2]);

      for (let i = 0; i < 2; i++) {
        if (diagonals[i][0] === diagonals[i][1] && diagonals[i][0] === diagonals[i][2]) {
          winner = diagonals[i][0];
          break;
        }
      }
    }

    if (winner) {
      this.isOverGame = true;
      alert('Победил '+ winner + '!');
    } else if (this.state.every(row => row.every(cell => cell !== null))) {
      this.isOverGame = true;
      alert("Ничья!");
    } else {
      this.mode = this.mode === "x" ? "o" : "x";
    }
  }
}

const gameField = new GameField();

while (!gameField.isOverGame) {
  const row = prompt("Введите номер строки: (0, 1, 2)");
  const col = prompt("Введите номер столбца: (0, 1, 2)");
  console.log("Состояние поля:" + gameField.getGameFieldStatus());

  if (gameField.FieldCellValue(row, col)) {
    gameField.checkForWinner();
  }
}

console.log("Игра окончена!");



